import os
import urllib
import datetime
import logging

import jinja2
import webapp2

from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.api import images
from google.appengine.ext import ndb


class Category(ndb.Model):
    name = ndb.StringProperty()
    description = ndb.StringProperty()


class Article(ndb.Model):
    title = ndb.StringProperty()
    imageurl = ndb.StringProperty()
    banner = ndb.StringProperty()
    intro = ndb.StringProperty()
    subcontent = ndb.TextProperty()
    content = ndb.TextProperty()
    date = ndb.DateProperty()
    category = ndb.KeyProperty(kind=Category)


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])


class MyPage(webapp2.RequestHandler):

    def get(self):

        categories = Category.query().fetch()
        articles = Article.query().fetch()
        articles_query = Article.query().order(-Article.date)
        template_values = {'articles': articles, 'categories': categories}
        template = JINJA_ENVIRONMENT.get_template('index.html')

        self.response.write(template.render(template_values))


class DetailPage(webapp2.RequestHandler):

    def get(self):

        categories = Category.query().fetch()
        articles = Article.query().fetch()
        articles_query = Article.query().order(-Article.date)
        template_values = {'articles': articles, 'categories': categories}
        template = JINJA_ENVIRONMENT.get_template('detail.html')

        self.response.write(template.render(template_values))


class ArticlePage(webapp2.RequestHandler):

    def get(self):

        categories = Category.query().fetch()
        articles = Article.query().fetch()

        articles_query = Article.query().order(-Article.date)

        template_values = {'articles': articles, 'categories': categories}
        template = JINJA_ENVIRONMENT.get_template('addArticle.html')
        self.response.write(template.render(template_values))

    def post(self):

        article = Article()
        logging.info("News tag is: %s", article.category)

        title = self.request.get('title')
        intro = self.request.get('intro')
        content = self.request.get('content')
        subcontent = self.request.get('subcontent')
        imageurl = self.request.get('imageurl')
        banner = self.request.get('banner')

        categorykey = self.request.get('category')
        category = Category.get_by_id(int(categorykey))

        article.title = title
        article.intro = intro
        article.category = category.key
        article.content = content
        article.subcontent = subcontent
        article.imageurl = imageurl
        article.banner = banner
        day = self.request.get('day')
        month = self.request.get('month')
        year = self.request.get('year')
        article.date = datetime.date(
            year=int(year), month=int(month), day=int(day))
        article.put()

        logging.info("Added articles: %s", article.key)
        self.redirect('/Article')


class ArticleEdit(webapp2.RequestHandler):

    def get(self, id):
        article = Article.query().fetch()

        article = Article.get_by_id(int(id))
        categories = Category.query().fetch()
        articles_query = Article.query().order(-Article.date)

        template_values = {'article': article, 'categories': categories}
        template = JINJA_ENVIRONMENT.get_template(
            'articleEdit.html')
        self.response.write(template.render(template_values))

    def post(self, id):
        title = self.request.get('title')
        intro = self.request.get('intro')
        content = self.request.get('content')
        subcontent = self.request.get('subcontent')
        categorykey = self.request.get('category')
        category = Category.get_by_id(int(categorykey))
        imageurl = self.request.get('imageurl')
        banner = self.request.get('banner')

        article = Article.get_by_id(int(id))

        article.title = title
        article.intro = intro
        article.category = category.key
        article.content = content
        article.subcontent = subcontent
        article.imageurl = imageurl
        article.banner = banner
        day = self.request.get('day')
        month = self.request.get('month')
        year = self.request.get('year')
        article.date = datetime.date(
            year=int(year), month=int(month), day=int(day))

        article.put()
        self.redirect('/Article')


class ArticleDelete(webapp2.RequestHandler):

    def get(self, id):

        template = JINJA_ENVIRONMENT.get_template(
            'articleDelete.html')

        article = Article.get_by_id(int(id))
        template_values = {'article': article}
        self.response.write(template.render(template_values))

    def post(self, id):

        article = Article.get_by_id(int(id))
        article.key.delete()
        self.redirect('/Article')


class CategoryPage(webapp2.RequestHandler):

    def get(self):

        template = JINJA_ENVIRONMENT.get_template('categories.html')
        categories = Category.query().fetch()

        template_values = {'categories': categories}
        self.response.write(template.render(template_values))

    def post(self):

        name = self.request.get('name')
        description = self.request.get('description')
        category = Category()
        category.name = name
        category.description = description

        category.put()

        self.redirect('/Category')


class CategoryEdit(webapp2.RequestHandler):

    def get(self, id):

        template = JINJA_ENVIRONMENT.get_template('categoryEdit.html')

        category = Category.get_by_id(int(id))

        template_values = {'category': category}
        self.response.write(template.render(template_values))

    def post(self, id):

        name = self.request.get('name')
        description = self.request.get('description')

        category = Category.get_by_id(int(id))
        category.name = name
        category.description = description

        category.put()
        self.redirect('/Category')


class CategoryDelete(webapp2.RequestHandler):

    def get(self, id):

        template = JINJA_ENVIRONMENT.get_template('categoryDelete.html')
        category = Category.get_by_id(int(id))
        template_values = {'category': category}
        self.response.write(template.render(template_values))

    def post(self, id):

        category = Category.get_by_id(int(id))
        category.key.delete()
        self.redirect('/Category')


application = webapp2.WSGIApplication([
    ('/', MyPage),
    ('/Details', DetailPage),
    ('/Article', ArticlePage),
    (r'/ArticleEdit/(\d+)', ArticleEdit),
    (r'/ArticleDelete/(\d+)',
     ArticleDelete),
    ('/Category', CategoryPage),
    (r'/CategoryEdit/(\d+)', CategoryEdit),
    (r'/CategoryDelete/(\d+)', CategoryDelete),
], debug=True)
